![](./mus.svg)

Mus
===

Mus is a headless audio player daemon for full-album FLACs with embedded cue sheets.

Mus expects your albums to all be in one folder, and to have proper metadata, and for the cue sheets to be in the format used by other popular audio players, and that the cues have artist and track labels.

It's:
- convenient
- simple
- not distracting
- respectful of system resources
- GUI-less - no desktop environment required
- only for linux

It does exactly one thing and nothing extra or fancy.

Usage Example
=============
```bash
mus some key words # mus will search your music directory for matches
                   # and play what it finds, or sass at you if it fails

# ... it launches a quiet daemon and releases your terminal ...
# ... and then you do a bunch of other stuff while your music plays ...
# ... and mus doesn't spam your console or desktop notifications ...
# ... and then you want to ...

mus -t 10          # skip to track 10

# ... and then the album is over, so mus quietly shuts down ...
# ... and then you can't decide what to listen to next ...

mus                # mus just picks a random album for you

# ... but you don't like the annoying intro track ...

mus -f             # fast-forward to the next track

# ... and so on ...
```

Full Usage
==========
```
mus :: a headless music player for full-album flacs

usage:
mus [options] [command] <search keyword>...

options:
mus -h                # show short help
mus --dir <directory> # set the music directory
mus --help            # this long help text
mus -v                # verbose output (for debugging, while in interactive)
mus -i                # enter an interactive shell (mostly for debugging)
mus -q                # no voice feedback*
mus -l                # turn voice feedback back on (if currently quiet)

send a command to the player while running:
mus (-p|-r|-s|-f|-b)  # pause, resume, stop, skip forward, or skip back
mus --tracks          # vocally recite tracks for the currently playing album
mut -t <number>       # play starting from track 10 on the current album

searching for albums (basic keyword matching against filenames):
mus zep iv            # play Led Zeppelin - Led Zeppelin IV
mus sma siam          # play Smashing Pumpkins - Siamese Dream
mus fear innoc        # play Tool - Fear Innoculum
mus                   # play a random album from your music directory

* voice feedback is on by default but requires espeak
```

Dependencies
============
- Rust programming language & toolchain
- make (or just manually enter the couple lines in the makefile)
- espeak (for depressed robot voice feedback)

Install
=======
```bash
git clone git@gitlab.com:nphyx/mus.git
make && sudo make install
```

Configure
=========
```bash
alias mus="mus --dir=<your flac album directory> [whatever other options]"
```
(put that in your ~/.bashrc or ~/.zshrc or whatever shell you use)

Troubleshooting
===============
Mus puts a few files in /tmp: `mus.in`, `mus.out`, `mus.pid`, `mus.err`. They're pretty much what they sound like. Delete them if something catastrophic happens and doesn't clean up, and then refuses to start. This shouldn't happen too often.

Cross-platform, playing other formats, GUI frontend, metadata management, etc.
------------------------------------------------------------------------------
You're looking for [DeaDBeeF](https://deadbeef.sourceforge.io/).

Or if you're on Android, [Poweramp](https://powerampapp.com/) works.

Or if you're on Windows, [Foobar2000](https://www.foobar2000.org/) is pretty OK too.

Or if you're on Mac or iOS, you probably already have something Apple mandated for you.

TODO
====
- Figure out better buffering to avoid skips (happens occaisonally, annoying)
- Respect replay gain metadata (figure out replay gain algorithm)
- Use unix sockets instead of janky text file
- Fix miscellaneous bugs (esp. to do with conflicting options)
- Use a cargo build script (eliminate dependency on make)
- Maybe build-in speech synthesis? Only if this can be done with almost no bloat, which is probably impossible without dynamic linking

Contributing
============
Probably don't, unless you can address something in the TODO list. Mus does not want any more features. Feel free to fork though.

License
=======
See [LICENSE](./LICENSE)
