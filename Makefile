SHELL:=/bin/bash
# could probably use cargo's own build system, but then I'd have to learn about it...
target := $(shell rustc -Vv | awk '/host:/{ print $$2 }' 2> /dev/null)
opts := -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target $(target)
all: build 
build:
	cargo +nightly build $(opts) --release
	cd target; rm make; ln -s $(target) make
debug:
	cargo +nightly build
bloat:
	cargo +nightly bloat $(opts) --release
install:
	cp target/make/release/mus /usr/local/bin/
	cp mus.desktop /usr/share/applications/
	cp mus.svg /usr/share/icons/hicolor/scalable/apps/mus.svg
