extern crate daemonize;
extern crate pico_args;
extern crate rand;
extern crate symphonia;
extern crate symphonia_bundle_flac;

mod options;
mod play;

use daemonize::Daemonize;
use options::*;
use std::fs::File;
use std::io::{prelude::*, BufRead, BufReader, Error, ErrorKind, LineWriter};
use std::path::Path; // use std::time::Duration;
use std::sync::mpsc::{Receiver, Sender};

const PID_FILE: &str = "/tmp/mus.pid";
const IN_FILE: &str = "/tmp/mus.in";
const OUT_FILE: &str = "/tmp/mus.out";
const ERR_FILE: &str = "/tmp/mus.err";

fn clean_up() {
    std::fs::remove_file(IN_FILE).expect("couldn't remove input file");
    std::fs::remove_file(OUT_FILE).expect("couldn't remove out file");
    std::fs::remove_file(ERR_FILE).expect("couldn't remove err file");
    std::fs::remove_file(PID_FILE).expect("couldn't remove pid file");
}

fn read_command(input: String) -> play::PlayerCommand {
    use play::PlayerCommand;
    match &input[..] {
        "pause" => PlayerCommand::Pause,
        "play" => PlayerCommand::Play,
        "random" => PlayerCommand::Random,
        "resume" => PlayerCommand::Resume,
        "stop" => PlayerCommand::Stop,
        "time" => PlayerCommand::Time,
        "forward" => PlayerCommand::SkipForward,
        "back" => PlayerCommand::SkipBack,
        "quiet" => PlayerCommand::Quiet(true),
        "loud" => PlayerCommand::Quiet(false),
        "tracks" => PlayerCommand::ReciteTracks,
        "help" => {
            println!(
                r#"
try play, pause, stop, random, next, prev, or any search text"#
            );
            PlayerCommand::None
        }
        "" => PlayerCommand::None,
        _ => {
            if input.contains("track") {
                let split: Vec<String> = input.split(' ').map(|s| s.to_string()).collect();
                if split.len() == 2 && split[0] == "track" {
                    let n: usize = split[1].as_str().parse().expect("bad track number");
                    return PlayerCommand::SelectTrack(n);
                }
            }
            PlayerCommand::Search(input)
        }
    }
}

fn write_command_from_opts(opts: MusOptions) -> std::io::Result<()> {
    use play::PlayerCommand::*;
    if opts.verbose {
        println!("found pid, passing command {:?}", opts.command);
    }
    let file = File::create(IN_FILE)?;
    let mut writer = LineWriter::new(file);
    if opts.quiet {
        writer.write_all(b"quiet\n")?;
    }
    if let Some(dir) = opts.music_dir {
        writer.write_all(format!("dir {}\n", dir).as_bytes())?;
    }
    match opts.command {
        Search(search) => {
            writer.write_all(search.as_bytes())?;
            writer.write_all(b"\n")?;
        }
        Quiet(q) => {
            if q {
                writer.write_all(b"quiet\n")?;
            } else {
                writer.write_all(b"loud\n")?;
            }
        }
        Stop => writer.write_all(b"stop\n")?,
        Pause => writer.write_all(b"pause\n")?,
        Resume => writer.write_all(b"resume\n")?,
        Random => writer.write_all(b"random\n")?,
        ReciteTracks => writer.write_all(b"tracks\n")?,
        SkipForward => writer.write_all(b"forward\n")?,
        SkipBack => writer.write_all(b"back\n")?,
        SelectTrack(n) => writer.write_all(format!("track {}\n", n).as_bytes())?,
        _ => {}
    }
    writer.flush()?;
    Ok(())
}

fn loop_interactive(
    opts: MusOptions,
    handler_tx: Sender<play::PlayerCommand>,
    handler_rx: Receiver<play::PlayerMessage>,
) -> Result<(), std::io::Error> {
    use play::PlayerCommand;
    let mut waiting_state_change = false;
    'main: loop {
        while let Ok(msg) = handler_rx.try_recv() {
            match msg {
                play::PlayerMessage::State(state) => {
                    waiting_state_change = false;
                    if opts.verbose {
                        println!("received state update {:?}", state);
                    }
                    if state == play::PlayerState::Stopped {
                        break 'main;
                    }
                }
                play::PlayerMessage::Notice(msg) => {
                    if opts.verbose {
                        println!("{}", msg);
                    }
                }
            }
        }

        let mut line = String::new();
        if !waiting_state_change {
            if std::io::stdin().read_line(&mut line).is_err() {
                std::thread::sleep(std::time::Duration::from_millis(25));
                continue;
            }
            if line.ends_with('\n') {
                line.pop();
            }

            let cmd = read_command(line);
            if cmd == PlayerCommand::None {
                continue;
            }

            if cmd == PlayerCommand::Stop {
                waiting_state_change = true;
            }

            handler_tx
                .send(cmd)
                .map_err(|_| Error::new(ErrorKind::Other, "couldn't send command"))?;
        }
    }
    Ok(())
}

fn loop_daemon(
    opts: MusOptions,
    handler_tx: Sender<play::PlayerCommand>,
    handler_rx: Receiver<play::PlayerMessage>,
) -> Result<(), std::io::Error> {
    File::create(IN_FILE)?;
    'main: loop {
        let mut got = false;
        std::thread::sleep(std::time::Duration::from_millis(25));
        let reader = BufReader::new(File::open(IN_FILE)?);
        for line in reader.lines() {
            got = true;

            let cmd = read_command(line?);

            handler_tx
                .send(cmd)
                .map_err(|_| Error::new(ErrorKind::Other, "couldn't send command"))?;
        }
        if got {
            File::create(IN_FILE)?; // truncates the file for next read
        }

        while let Ok(msg) = handler_rx.try_recv() {
            match msg {
                play::PlayerMessage::State(state) => {
                    if state == play::PlayerState::Stopped {
                        break 'main Ok(());
                    }
                }
                play::PlayerMessage::Notice(msg) => {
                    if opts.verbose {
                        println!("{}", msg);
                    }
                }
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    let opts = MusOptions::from_args();
    match opts.help {
        HelpType::Short => {
            show_short_help();
            return Ok(());
        }
        HelpType::Long => {
            show_long_help();
            return Ok(());
        }
        HelpType::None => {}
    }

    if Path::new(PID_FILE).exists() {
        // there's already a daemon running
        // just write a command to the input "socket" and bail
        return write_command_from_opts(opts);
    }

    if opts.verbose {
        println!("running commmand {:?}", opts.command);
    }

    if !opts.interactive {
        // daemonize and read from "socket" at /tmp/mus.in
        let stdout = File::create(OUT_FILE)?;
        let stderr = File::create(ERR_FILE)?;
        let mut da = Daemonize::new()
            .pid_file(PID_FILE)
            .stdout(stdout)
            .stderr(stderr);
        if opts.verbose {
            da = da.exit_action(|| println!("daemonizing..."));
            match da.start() {
                Ok(_) => println!("daemonized!"),
                Err(e) => eprintln!("Error, {}", e),
            }
        } else {
            da.start().expect("FAILURE");
        }
    }

    /* let handle = */
    let (handler_tx, player_rx) = std::sync::mpsc::channel::<play::PlayerCommand>();
    let (player_tx, handler_rx) = std::sync::mpsc::channel::<play::PlayerMessage>();
    let mut player = play::Player::new(opts.clone(), player_tx, player_rx);
    std::thread::spawn(move || {
        match player.start() {
            Ok(()) => { // when playback ends
            }
            Err(e) => {
                eprintln!("error at end of stream {}", e);
            } //.expect("player couldn't start");
        }
        // clean up when done
        clean_up();
    });
    handler_tx
        .send(opts.clone().command)
        .expect("couldn't send command to player :(");

    if opts.interactive {
        println!("entering interactive mode...");
        loop_interactive(opts, handler_tx, handler_rx)?;
    } else {
        loop_daemon(opts, handler_tx, handler_rx)?;
        // clean up if the loop exits
        clean_up();
    }
    Ok(())
}
