/// struct and functions for dealing with CLI input
use crate::play::PlayerCommand;
#[derive(Debug, Copy, Clone)]
pub enum HelpType {
    Short,
    Long,
    None,
}

#[derive(Debug, Clone)]
pub struct MusOptions {
    pub command: PlayerCommand,
    pub music_dir: Option<String>,
    pub help: HelpType,
    pub quiet: bool,
    pub verbose: bool,
    pub interactive: bool,
}

impl MusOptions {
    pub fn from_args() -> MusOptions {
        use crate::play::PlayerCommand::*;
        let mut args = pico_args::Arguments::from_env();
        let help_short = args.contains("-h");
        let help_long = args.contains("--help");

        if help_short || help_long {
            return MusOptions {
                command: None,
                music_dir: std::option::Option::None,
                help: if help_short {
                    HelpType::Short
                } else {
                    HelpType::Long
                },
                quiet: false,
                verbose: false,
                interactive: false,
            };
        }

        let music_dir: Option<String> = args
            .opt_value_from_str(["-d", "--dir"])
            .unwrap_or(std::option::Option::None);
        let interactive = args.contains("-i");
        let quiet = args.contains("-q");
        let loud = args.contains("-l");
        let verbose = args.contains("-v");
        let pause = args.contains("-p");
        let resume = args.contains("-r");
        let stop = args.contains("-s");
        let tracks = args.contains("--tracks");
        let track_no: usize = args
            .opt_value_from_str("-t")
            .unwrap_or(std::option::Option::None)
            .unwrap_or(0);
        let forward = args.contains("-f");
        let back = args.contains("-b");
        let search: Vec<String> = args.free().unwrap_or_default();

        let command: PlayerCommand;
        if stop {
            command = Stop;
        } else if forward {
            command = SkipForward;
        } else if back {
            command = SkipBack;
        } else if pause {
            command = Pause;
        } else if resume {
            command = Resume;
        } else if quiet {
            command = Quiet(true);
        } else if loud {
            command = Quiet(false);
        } else if tracks {
            command = ReciteTracks;
        } else if track_no > 0 {
            command = SelectTrack(track_no);
        } else if !search.is_empty() {
            command = Search(search.join(" "));
        } else {
            command = Random;
        }

        MusOptions {
            command,
            music_dir,
            help: HelpType::None,
            quiet,
            verbose,
            interactive,
        }
    }
}

pub fn show_long_help() {
    println!(
        r#"
mus :: a headless music player for full-album flacs

usage:
mus [options] [command] <search keyword>...

options:
mus -h                # show short help
mus --help            # this long help text
mus -v                # verbose output (for debugging)**
mus -i                # enter an interactive shell (mostly for debugging)
mus -q                # no voice feedback*
mus -l                # turn voice feedback back on (if currently quiet)
mus --dir <directory> # set the music directory

send a command to the player while running:
mus (-p|-r|-s|-f|-b)  # pause, resume, stop, skip forward, or skip back
mus --tracks          # vocally recite tracks for the currently playing album
mut -t <number>       # play starting from track 10 on the current album

searching for albums (basic keyword matching against filenames):
mus zep iv            # play Led Zeppelin - Led Zeppelin IV
mus sma siam          # play Smashing Pumpkins - Siamese Dream
mus fear innoc        # play Tool - Fear Innoculum
mus                   # play a random album from your music directory

* voice feedback is on by default but requires espeak
  - install it if you don't hear speech
** verbose output has to be toggled on at initial launch for the daemon to do verbose logs"#
    );
}

pub fn show_short_help() {
    println!("mus [options] [command] <search keyword>... # mus --help for full text");
}
