// Borrowed wholesale from:
//
// Symphonia
// Copyright (c) 2019 The Project Symphonia Developers.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::io::{Error, ErrorKind};
use symphonia::core::audio::*;
use symphonia::core::audio::{AudioBufferRef, SignalSpec};
use symphonia::core::units::Duration;

use libpulse_binding as pulse;
use libpulse_simple_binding as psimple;

pub trait AudioOutput {
    fn write(&mut self, decoded: AudioBufferRef<'_>) -> Result<(), Error>;
    fn flush(&mut self);
}

pub struct PulseAudioOutput {
    pa: psimple::Simple,
    sample_buf: RawSampleBuffer<f32>,
}

impl PulseAudioOutput {
    pub fn try_open(spec: SignalSpec, duration: Duration) -> Result<Box<dyn AudioOutput>, Error> {
        // An interleaved buffer is required to send data to PulseAudio. Use a SampleBuffer to
        // move data between Symphonia AudioBuffers and the byte buffers required by PulseAudio.
        let sample_buf = RawSampleBuffer::<f32>::new(duration, spec);

        // Create a PulseAudio stream specification.
        let pa_spec = pulse::sample::Spec {
            format: pulse::sample::Format::FLOAT32NE,
            channels: spec.channels.count() as u8,
            rate: spec.rate,
        };

        assert!(pa_spec.is_valid());

        // PulseAudio seems to not play very short audio buffers, use thse custom buffer
        // attributes for very short audio streams.
        //
        // let pa_buf_attr = pulse::def::BufferAttr {
        //     maxlength: std::u32::MAX,
        //     tlength: 1024,
        //     prebuf: std::u32::MAX,
        //     minreq: std::u32::MAX,
        //     fragsize: std::u32::MAX,
        // };

        // Create a PulseAudio connection.
        let pa_result = psimple::Simple::new(
            None,                               // Use default server
            "mus",                              // Application name
            pulse::stream::Direction::Playback, // Playback stream
            None,                               // Default playback device
            "Music",                            // Description of the stream
            &pa_spec,                           // Signal specificaiton
            None,                               // Default channel map
            None,                               // Custom buffering attributes
        );

        match pa_result {
            Ok(pa) => Ok(Box::new(PulseAudioOutput { pa, sample_buf })),
            Err(_) => Err(Error::new(
                ErrorKind::Other,
                "audio output stream open error",
            )),
        }
    }
}

impl AudioOutput for PulseAudioOutput {
    fn write(&mut self, decoded: AudioBufferRef<'_>) -> Result<(), Error> {
        // Interleave samples from the audio buffer into the sample buffer.
        self.sample_buf.copy_interleaved_ref(decoded);

        // Write interleaved samples to PulseAudio.
        match self.pa.write(self.sample_buf.as_bytes()) {
            Err(_) => Err(Error::new(
                ErrorKind::Other,
                "audio output stream write error",
            )),
            _ => Ok(()),
        }
    }

    fn flush(&mut self) {
        // Flush is best-effort, ignore the returned result.
        let _ = self.pa.drain();
    }
}

pub fn try_open(spec: SignalSpec, duration: Duration) -> Result<Box<dyn AudioOutput>, Error> {
    PulseAudioOutput::try_open(spec, duration)
}
