mod pulse;
mod album;

use album::Album;

use std::io::{Error, ErrorKind};
use std::path::Path;
use std::process::Command;
use std::sync::mpsc::{Receiver, Sender};
use std::time::{Duration, Instant};
use symphonia::core::{
    audio::SignalSpec,
    codecs::{Decoder, DecoderOptions},
    formats::{FormatOptions, SeekTo},
    io::MediaSourceStream,
    meta::MetadataOptions,
    probe::{Hint, ProbeResult},
    units::Time,
};
use symphonia_bundle_flac::FlacDecoder;

fn time_from_dur(dur: Duration) -> Time {
    Time {
        seconds: dur.as_secs(),
        frac: dur.subsec_nanos() as f64 / 1_000_000_000.0,
    }
}

/// process the music_dir option from MusOptions
pub fn find_music_dir(dir: Option<String>) -> String {
    match dir {
        Some(dir) => dir,
        _ => {
            #[allow(deprecated)]
            let home_path = std::env::home_dir().expect("No home directory found :(");
            let music_path = home_path.join("Music");
            music_path.to_str().unwrap().into()
        }
    }
}

fn probe_flac(file_path: &std::path::Path) -> Result<ProbeResult, std::io::Error> {
    let mut hint = Hint::new();
    hint.with_extension("flac");
    if let Ok(file) = std::fs::File::open(file_path) {
        let format_opts: FormatOptions = Default::default();
        let metadata_opts: MetadataOptions = Default::default();
        let mss = MediaSourceStream::new(Box::new(file));
        match symphonia::default::get_probe().format(&hint, mss, &format_opts, &metadata_opts) {
            Ok(probed) => return Ok(probed),
            _ => return Err(Error::new(ErrorKind::Other, "symphonia failed to probe :(")),
        }
    }
    Err(Error::new(ErrorKind::Other, "failed to read file :("))
}

fn time_string(dur: Duration) -> String {
    let hours = (dur.as_secs() as f32 / (60.0 * 60.0)).floor();
    let minutes = (dur.as_secs() as f32 / 60.0).floor() - (hours * 60.0);
    let seconds = (dur.as_secs() as f32).floor() - (hours * 60.0 * 60.0) - (minutes * 60.0);
    let hr_string = if hours > 0.0 {
        format!("{} hours", hours)
    } else {
        "".to_string()
    };

    let min_string = if minutes > 0.0 {
        format!("{} minutes", minutes)
    } else {
        "".to_string()
    };

    let sec_string = if seconds > 0.0 {
        format!("{} seconds", seconds)
    } else {
        "".to_string()
    };

    let time = time_from_dur(dur);

    format!(
        "{} {} {} :: ({})",
        hr_string, min_string, sec_string, time.seconds
    )
}

#[derive(Eq, PartialEq, Clone, Debug)]
pub enum PlayerMessage {
    State(PlayerState),
    Notice(String),
}

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub enum PlayerState {
    /// currently emitting audio (or trying to)
    Playing,
    /// setting up to pause
    Pausing,
    /// have album loaded but in paused state
    Paused(Duration),
    /// getting ready to play
    Starting,
    /// beginning to stop
    Stopping,
    /// really stopped, as in shutting down
    Stopped,
    /// just opened a new album and haven't started playback yet
    NewAlbum,
    /// changing tracks
    NewTrack,
    /// no album loaded and waiting for input
    Idle,
    // Error,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum PlayerCommand {
    Pause,
    Play,
    Quiet(bool),
    Random,
    ReciteTracks,
    Resume,
    Search(String),
    SelectTrack(usize),
    SkipBack,
    SkipForward,
    Stop,
    Time,
    None,
}

pub struct Player {
    /// last selected track
    last_selected_track: usize,
    /// directory of album file
    music_dir: String,
    /// currently playing album
    now_playing: Option<Album>,
    receiver: Receiver<PlayerCommand>,
    transmitter: Sender<PlayerMessage>,
    /// whether to speak
    quiet: bool,
    pub state: PlayerState,
}

impl Player {
    pub fn new(
        opts: crate::options::MusOptions,
        transmitter: Sender<PlayerMessage>,
        receiver: Receiver<PlayerCommand>,
    ) -> Player {
        Player {
            last_selected_track: 0,
            music_dir: find_music_dir(opts.music_dir),
            now_playing: None,
            quiet: opts.quiet,
            receiver,
            state: PlayerState::Idle,
            transmitter,
        }
    }

    pub fn acquire_probe_and_decoder(
        &mut self,
    ) -> Result<(ProbeResult, FlacDecoder), std::io::Error> {
        let album = &self.now_playing.as_ref();
        let path = album.unwrap().file_path.clone();

        let probe = probe_flac(Path::new(&path))
            .map_err(|_| Error::new(ErrorKind::Other, "failed to probe album"))?;
        let stream = probe.format.default_stream().unwrap();
        let decode_options = DecoderOptions { verify: false };

        let decoder =
            FlacDecoder::try_new(&stream.codec_params, &decode_options)
                .map_err(|_| Error::new(ErrorKind::Other, "failed to decode album"))?;
        Ok((probe, decoder))
    }

    fn acquire_spec_and_duration(
        probe: &mut Option<Box<ProbeResult>>,
        decoder: &mut Option<Box<FlacDecoder>>,
    ) -> Result<(SignalSpec, usize), std::io::Error> {
        let packet = probe
            .as_mut()
            .unwrap()
            .format
            .next_packet()
            .map_err(|_| Error::new(ErrorKind::Other, "packet read error"))?;
        match decoder.as_mut().unwrap().decode(&packet) {
            Err(_) => Err(Error::new(
                ErrorKind::Other,
                "unknown fatal error while acquiring spec & duration",
            )),
            Ok(decoded) => Ok((*decoded.spec(), decoded.capacity())),
        }
    }

    pub fn start(&mut self) -> Result<(), std::io::Error> {
        let mut probe: Option<Box<ProbeResult>> = None;
        let mut decoder: Option<Box<FlacDecoder>> = None;
        let mut output: Option<Box<dyn pulse::AudioOutput>> = None;
        let mut last_start: Instant = Instant::now();
        let mut last_album_position: Duration = Duration::from_secs(0);
        let mut current_play_time: Duration;
        let mut packet_fails = 0;
        let mut decoder_fails = 0;
        loop {
            current_play_time = last_album_position + last_start.elapsed();
            // first check if we have any messages
            if let Ok(cmd) = self.receiver.try_recv() {
                self.handle(cmd, current_play_time)?;
            }

            match self.state {
                PlayerState::NewAlbum => {
                    last_album_position = Duration::from_secs(0);
                    self.send_state();
                    self.send_msg(time_string(current_play_time));
                    // we need to acquire a new probe and decoder on the newly
                    // loaded album
                    let probe_dec = self.acquire_probe_and_decoder()?;
                    probe = Some(Box::new(probe_dec.0));
                    decoder = Some(Box::new(probe_dec.1));
                    let spec_dur = Player::acquire_spec_and_duration(&mut probe, &mut decoder)?;
                    output = pulse::try_open(spec_dur.0, spec_dur.1 as u64).ok();
                    if let Some(album) = &self.now_playing {
                        self.speak(format!("playing {}, {}", album.cuesheet.get_performer(), album.cuesheet.get_title()));
                    }
                    // now we're ready to play
                    self.state = PlayerState::Starting;
                }
                PlayerState::NewTrack => {
                    self.send_state();
                    self.send_msg(time_string(current_play_time));
                    // changing tracks invalidates the decoder so let's rebuild all that
                    let probe_dec = self.acquire_probe_and_decoder()?;
                    probe = Some(Box::new(probe_dec.0));
                    decoder = Some(Box::new(probe_dec.1));
                    let spec_dur = Player::acquire_spec_and_duration(&mut probe, &mut decoder)?;
                    if let Some(index) = self.now_playing
                        .as_ref()
                        .unwrap()
                        .cuesheet.tracks.get(&self.last_selected_track)
                        .unwrap()
                        .indices
                        .last()
                    {
                        let (s, f) = index.point.as_time();
                        let seek_to = SeekTo::Time { time: Time::new(s, f) };
                        match probe.as_mut().unwrap().format.seek(seek_to) {
                            Ok(_) => {
                                output = pulse::try_open(spec_dur.0, spec_dur.1 as u64).ok();
                            }
                            Err(_) => {
                                self.speak("fail".into());
                                self.state = PlayerState::Idle;
                                continue;
                            }
                        }
                        last_album_position = index.point.as_duration();
                        self.state = PlayerState::Starting;
                    }
                    continue;
                }
                PlayerState::Stopped => {
                    self.send_state();
                    self.send_msg(time_string(current_play_time));
                    return Ok(());
                }
                PlayerState::Stopping => {
                    self.send_state();
                    self.send_msg(time_string(current_play_time));
                    self.speak("i don't like music either".into());
                    self.state = PlayerState::Stopped;
                }
                PlayerState::Pausing => {
                    self.send_state();
                    self.send_msg(time_string(current_play_time));
                    self.speak("i am pause".into());
                    last_album_position += last_start.elapsed();
                    self.state = PlayerState::Paused(last_album_position);
                }
                PlayerState::Paused(_) | PlayerState::Idle => {
                    std::thread::sleep(Duration::from_millis(25));
                    continue;
                }
                PlayerState::Starting => {
                    self.send_state();
                    self.send_msg(time_string(current_play_time));
                    last_start = Instant::now();
                    self.state = PlayerState::Playing;
                }
                PlayerState::Playing => {
                    // validate everything
                    if probe.is_none() || decoder.is_none() {
                        self.state = PlayerState::Idle;
                        continue;
                    }
                    // ok, keep playing
                    if let Ok(packet) = probe.as_mut().unwrap().format.next_packet() {
                        packet_fails = 0;
                        if let Ok(decoded) = decoder.as_mut().unwrap().decode(&packet) {
                            decoder_fails = 0;
                            match &mut output {
                                Some(o) => o.write(decoded).unwrap(),
                                None => {}
                            }
                        } else {
                            decoder_fails += 1;
                        }
                    } else {
                        packet_fails += 1;
                    }

                    if packet_fails > 100 || decoder_fails > 100 {
                        // something went really wrong, or maybe just end of album
                        // TODO find some other way of detecting end of file vs. actual error
                        // ... but we can still exit clean because we don't know if it was a real
                        // problem
                        return Ok(());
                    }
                } // end of playing
            } // end of state match
        } // end of loop
    }

    fn send_msg(&self, message: String) {
        self.transmitter.send(PlayerMessage::Notice(message)).ok();
        // don't really care if this fails, nothing bad happens
    }

    fn send_state(&self) {
        self.transmitter.send(PlayerMessage::State(self.state)).ok();
        // don't really care if this fails, worst case the interactive shell hiccups
    }

    pub fn add_album(&mut self, file_path: &Path) {
        let album = Album::from_flac(file_path.to_string_lossy().to_string());
        self.now_playing = Some(album);
    }

    pub fn handle(
        &mut self,
        cmd: PlayerCommand,
        current_play_time: Duration,
    ) -> Result<(), std::io::Error> {
        self.send_msg(format!("player handling {:?}", cmd));
        use PlayerCommand::*;
        match cmd {
            Stop => {
                if matches!(
                    self.state,
                    PlayerState::Playing | PlayerState::Paused(_) | PlayerState::Idle
                ) {
                    self.state = PlayerState::Stopping;
                } else {
                    self.speak("can't stop, won't stop".into());
                    return Ok(());
                }
            }
            Pause => {
                if self.state != PlayerState::Playing {
                    self.speak("don't tell me what to do".into());
                } else {
                    self.state = PlayerState::Pausing;
                }
            }
            Resume => match self.state {
                PlayerState::Paused(_) => {
                    self.speak("resuming".into());
                    self.state = PlayerState::Starting;
                }
                _ => {
                    self.speak("don't tell me what to do".into());
                }
            },
            Random => {
                self.random()?;
            }
            /*
            ChangeMusicDirectory(d) => {
                self.music_dir = find_music_dir(d);
                self.speak("music directory changed".into());
            }
            */
            SelectTrack(n) => {
                self.send_msg(format!("changing track to {}", n));
                if let Some(album) = &self.now_playing {
                    if let Some(track) = album.cuesheet.tracks.get(&n) {
                        self.state = PlayerState::NewTrack;
                        self.last_selected_track = n;
                        self.speak(format!("playing {}", track.get_title()));
                    } else {
                        self.speak(format!("i only have {} tracks", album.cuesheet.tracks.len()));
                    }
                } else {
                    self.speak("pick an album first".into());
                }
            }
            SkipForward => {
                if let Some(album) = &self.now_playing {
                    let cur = album.cuesheet.track_at(current_play_time);
                    self.send_msg(format!(
                        "skipping forward at time :: {} :: from {}: {}",
                        time_string(current_play_time),
                        cur,
                        album.cuesheet.tracks.get(&cur).unwrap().get_title(),
                    ));
                    let next = album.cuesheet.track_after(cur);
                    match next {
                        Some(track) => {
                            let dur = match track.indices.first() {
                                Some(index) => index.point.as_duration(),
                                Option::None => Default::default(),
                            };
                            self.send_msg(format!(
                                "found track {} at {}",
                                track.get_title(),
                                time_string(dur)
                            ));
                            self.state = PlayerState::NewTrack;
                            self.last_selected_track = track.number;
                            self.speak(format!("playing {}", track.get_title()));
                        }
                        std::option::Option::None => {
                            self.speak("i'm afraid i can't do that".into());
                        }
                    }
                } else {
                    self.speak("pick an album first".into());
                }
            }
            SkipBack => {
                if let Some(album) = &self.now_playing {
                    let cur = album.cuesheet.track_at(current_play_time);
                    self.send_msg(format!(
                        "skipping backward at time :: {} :: from {}: {}",
                        time_string(current_play_time),
                        cur,
                        album.cuesheet.tracks.get(&cur).unwrap().get_title(),
                    ));
                    let prev = album.cuesheet.track_before(cur);
                    match prev {
                        Some(track) => {
                            let dur = match track.indices.first() {
                                Some(index) => index.point.as_duration(),
                                Option::None => Default::default(),
                            };
                            self.send_msg(format!(
                                "found track {} at {}",
                                track.get_title(),
                                time_string(dur)
                            ));
                            self.state = PlayerState::NewTrack;
                            self.last_selected_track = track.number;
                            self.speak(format!("playing {}", track.get_title()));
                        }
                        std::option::Option::None => {
                            self.speak("i'm afraid i can't do that".into());
                        }
                    }
                } else {
                    self.speak("pick an album first".into());
                }
            }
            ReciteTracks => {
                if let Some(album) = &self.now_playing {
                    for track in album.cuesheet.tracks.values() {
                        self.speak(format!("{}. {}", track.number, &track.get_title()));
                    }
                }
            }
            Search(query) => {
                let fname = Path::new(&query);
                if fname.is_file() {
                    self.add_album(fname);
                    self.state = PlayerState::NewAlbum;
                } else {
                    let keys = query.split(' ').map(|s| s.to_string()).collect();
                    self.send_msg(format!("searching for {:?}", keys));
                    self.search(keys)?;
                }
            }
            _ => {
                // unsupported/unrecognized commands
                self.speak("your shit is fucked".into());
            }
        }
        Ok(())
    }

    pub fn search(&mut self, keys: Vec<String>) -> Result<(), std::io::Error> {
        let tmp_path = self.music_dir.clone();
        let dir = Path::new(&tmp_path);
        if dir.is_dir() {
            'entries: for entry in std::fs::read_dir(dir)? {
                let file = entry?;
                let the_path = file.path();
                let the_name = the_path
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_lowercase();
                for entry in &keys {
                    if !the_name.contains(&entry.to_lowercase()) {
                        continue 'entries;
                    }
                }
                self.add_album(&the_path);
                self.state = PlayerState::NewAlbum;
                return Ok(());
            }
        }
        self.speak("your shit is fucked".into());
        Ok(())
    }

    pub fn random(&mut self) -> Result<(), std::io::Error> {
        use rand::rngs::SmallRng;
        use rand::seq::SliceRandom;
        use rand::SeedableRng;
        let tmp_path = self.music_dir.clone();
        let dir = Path::new(&tmp_path);
        if dir.is_dir() {
            let ext: &std::ffi::OsStr = std::ffi::OsStr::new("flac");
            let mut entries: Vec<std::path::PathBuf> = std::fs::read_dir(dir)?
                .map(|d| d.unwrap().path())
                .filter(|p| p.extension() == Some(ext))
                .collect();
            let mut rng = SmallRng::from_entropy();
            entries.shuffle(&mut rng);
            let choice = entries.pop();
            if let Some(album) = choice {
                self.add_album(&album);
                self.state = PlayerState::NewAlbum;
                return Ok(());
            }
        }
        self.speak("settle for nothing".into());
        Ok(())
    }

    fn speak(&self, line: String) {
        self.send_msg(line.clone());
        if !self.quiet {
            Command::new("espeak")
                .arg(line)
                .output()
                .expect("don't speak // I know just what you're thinking");
        }
    }
}
