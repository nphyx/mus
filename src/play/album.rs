use cuet::CueSheet;
use metaflac::{Tag,BlockType,Block};

#[derive(Debug)]
struct AlbumError;

pub fn read_cue_from_flac(filename: &String) -> CueSheet {
    if let Ok(tag) = Tag::read_from_path(&filename) {
        let mut blocks = tag.get_blocks(BlockType::VorbisComment);
        if let Some(Block::VorbisComment(block)) = blocks.next() {
            if let Some(sheet) = block.get("CUESHEET") {
                return CueSheet::from(sheet.first().unwrap());
            }
        }
    }
    Default::default()
}

#[derive(Default,Debug,Clone,PartialEq,Eq)]
pub struct Album {
    pub cuesheet: CueSheet,
    pub file_path: String,
}

impl Album {
    pub fn from_flac(file_path: String) -> Album {
        Self {
            cuesheet: read_cue_from_flac(&file_path),
            file_path,
        }
    }
}
